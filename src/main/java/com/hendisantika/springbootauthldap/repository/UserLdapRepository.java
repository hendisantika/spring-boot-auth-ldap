package com.hendisantika.springbootauthldap.repository;

import com.hendisantika.springbootauthldap.domain.UserLdap;
import org.springframework.data.ldap.repository.LdapRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-auth-ldap
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 27/05/18
 * Time: 16.12
 * To change this template use File | Settings | File Templates.
 */
@Repository
public interface UserLdapRepository extends LdapRepository<UserLdap> {
    UserLdap findByUsernameAndPassword(String username, String password);
}
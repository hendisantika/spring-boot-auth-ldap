package com.hendisantika.springbootauthldap.config;

import com.hendisantika.springbootauthldap.domain.UserSession;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-auth-ldap
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 28/05/18
 * Time: 21.06
 * To change this template use File | Settings | File Templates.
 */
@Slf4j
@Component
public class CustomAuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler
        implements AuthenticationSuccessHandler {

    private final static String DEFAULT_REDIRECT_URL_AFTER_LOGIN = "/index";

    public CustomAuthenticationSuccessHandler() {
        super();
        setUseReferer(true);
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {

        UserSession authUser = (UserSession) authentication.getPrincipal();

        log.info("Logged in username: {}", authUser.getUsername());

//        Allow user to be redirected to the request resource after authentication
        SavedRequest savedRequest = (SavedRequest) request.getSession().getAttribute("SPRING_SECURITY_SAVED_REQUEST");
        String redirectUrl = savedRequest == null ? DEFAULT_REDIRECT_URL_AFTER_LOGIN : savedRequest.getRedirectUrl();

        response.sendRedirect(redirectUrl);

    }
}

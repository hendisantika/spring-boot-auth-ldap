package com.hendisantika.springbootauthldap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootAuthLdapApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootAuthLdapApplication.class, args);
    }
}
